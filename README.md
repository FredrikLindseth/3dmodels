# 3D models
My repository for my public 3D models to be 3D-printed. I usually design in FreeCAD and both STL and FreeCAD-models are included in their respective folders

## [Detergent measurement spoon](measurement_spoon)
<img src="measurement_spoon/measurementSpoon.jpeg" alt="The finished spoon" style="width: 100px;"/>

This is a measurement spoon for dishwasher detergent, made to measurement to have the correct dose for Sun/Finish-detergent.
The volume of the spoon is 17 ml which is the recommended dose.
The handle is angled out from the spoon for improved usability.
The spoon is 10 cm long and with ~30% infill the finished spoon weighs 9gram.

TODO
* One layer of plastic, from the cup, sticks out at the bakside of the handle when printed
* The handle should also be angled sideways for -even more- usability

## More to come
